/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rafael.rodrigo.carteiradecliente.bean;

import br.com.rafael.rodrigo.carteiradecliente.classe.Funcionario;
import br.com.rafael.rodrigo.carteiradecliente.dao.FuncionarioDAO;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Administrador
 */
@Named(value = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

    private Funcionario funcionario;
    private FuncionarioDAO dao;

    public LoginBean() {
    }

    @PostConstruct
    private void init() {
        this.funcionario = new Funcionario();
        this.dao = new FuncionarioDAO();
    }

    public void login() {

        if(dao.findByLogin(funcionario) != null){
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("PF('login').hide();") ; 
        }
        
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

}
