/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rafael.rodrigo.carteiradecliente.bean;

import br.com.rafael.rodrigo.carteiradecliente.classe.Autor;
import br.com.rafael.rodrigo.carteiradecliente.dao.AutorDAO;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "autorBean")
@ViewScoped
public class AutorBean extends Bean {
    
    private Autor autor;
    private AutorDAO dao;
    
    public AutorBean() {
    }
    
    @PostConstruct
    public void init() {
        this.autor = new Autor();
        this.dao = new AutorDAO();
    }
    
    public String getCodigo() {
        return this.autor.getId() == 0 ? "" : String.valueOf(this.autor.getId());
    }
    
    public void novo() {
        this.autor = new Autor();
    }
    
    public void salvar() {
        
        try {
            
            if (this.autor.getId() == 0) {
                dao.save(autor);
                addMessageInfo("Salvo com sucesso!");
            } else {
                dao.update(autor);
                addMessageInfo("Alterado com sucesso!");
            }
            
        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }
        
    }
    
    public void excluir(Autor autor) {
        try {
            
            dao.delete(autor.getId());
            addMessageInfo("Removido com sucesso!");
            
        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }
    }
 // A partir daqui   
    public Autor getAutor() {
        return autor;
    }
    
    public void setAutor(Autor autor) {
        this.autor = autor;
    }
    
    public List<Autor> getLista() {
        return this.dao.findAll();
    }
    
}
