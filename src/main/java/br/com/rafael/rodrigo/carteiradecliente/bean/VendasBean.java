/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rafael.rodrigo.carteiradecliente.bean;

import br.com.rafael.rodrigo.carteiradecliente.classe.Vendas;
import br.com.rafael.rodrigo.carteiradecliente.dao.VendasDAO;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author Administrador
 */

@Named(value = "livroBean")
   @ViewScoped
public class VendasBean extends Bean{
    private  Vendas vendas;
    private VendasDAO dao;

   public VendasBean() {
    }
    
    @PostConstruct
    public void init() {
        this.vendas = new Vendas();
        this.dao = new VendasDAO();
    }
    
    public String getCodigo() {
        return this.vendas.getId() == 0 ? "" : String.valueOf(this.vendas.getId());
    }
    
    public void novo() {
        this.vendas = new Vendas();
    }
    
    public void salvar() {
        
        try {
            
            if (this.vendas.getId() == 0) {
                dao.save(vendas);
                addMessageInfo("Salvo com sucesso!");
            } else {
                dao.update(vendas);
                addMessageInfo("Alterado com sucesso!");
            }
            
        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }
        
    }
    
    public void excluir(Vendas vendas) {
        try {
            
            dao.delete(vendas.getId());
            addMessageInfo("Removido com sucesso!");
            
        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }
    }
 // A partir daqui   
    public Vendas getLivro() {
        return vendas;
    }
    
    public void setLivor(Vendas livro) {
        this.vendas = livro;
    }
    
    public List<Vendas> getLista() {
        return this.dao.findAll();
    }
    
}
