/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rafael.rodrigo.carteiradecliente.bean;

import br.com.rafael.rodrigo.carteiradecliente.classe.Funcionario;
import br.com.rafael.rodrigo.carteiradecliente.dao.FuncionarioDAO;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;


/**
 *
 * @author Diamond
 */
@Named(value = "funcionarioBean")
@ViewScoped
public class FuncionarioBean extends Bean {

    private Funcionario funcionario;
    private FuncionarioDAO dao;
    
     public FuncionarioBean (){
    }
    
    @PostConstruct
    public void init (){
        this.funcionario = new Funcionario();
        this.dao = new FuncionarioDAO();
    }
    
    public String getCodigo(){
       return  this.funcionario.getId() == 0 ? "" : String.valueOf(this.funcionario.getId());
        
      
    }
    
     public String novo() {
        this.funcionario = new Funcionario();
        return null ; 
    }
     
     public void pesquisar (){
         
     }
     
    public void salvar() {
        
        try {
            
            if (this.funcionario.getId() == 0) {
                dao.save(funcionario);
                addMessageInfo("Salvo com sucesso!");
               
                
            } else {
                dao.update(funcionario);
                addMessageInfo("Alterado com sucesso!");
            }
            
            
        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }
        
    }
     
     public void excluir(Funcionario funcionario) {
        try {
            
            dao.delete(funcionario.getId());
            addMessageInfo("Removido com sucesso!");
            
        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }
    }
     
     public void removeBean(String bean){
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove(bean);
     } 
   
    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }
   
    public List<Funcionario> getLista() {
        return this.dao.findAll();
    }
    
    }
    
   
    
    
    
    

//    public FuncionarioBean() {
//    }
//
//    @PostConstruct
//    public void init() {
//        this.funcionario = new Funcionario();
//        this.dao = new FuncionarioDAO();
//    }
//
//    public String getCodigo() {
//        return this.funcionario.getId() == 0 ? "" : String.valueOf(this.funcionario.getId());
//    }
//
//    public void novo() {
//        this.funcionario = new Funcionario();
//    }
//
//    public void salvar() {
//
//        try {
//
//            if (this.funcionario.getId() == 0) {
//                dao.save(funcionario);
//                addMessageInfo("Salvo com sucesso!");
//            } else {
//                dao.update(funcionario);
//                addMessageInfo("Alterado com sucesso!");
//            }
//
//        } catch (Exception ex) {
//            addMessageInfo(ex.getMessage());
//        }
//
//    }
//
//    public void excluir(Funcionario funcionario) {
//        try {
//
//            dao.delete(funcionario.getId());
//            addMessageInfo("Removido com sucesso!");
//
//        } catch (Exception ex) {
//            addMessageErro(ex.getMessage());
//        }
//    }
//    // A partir daqui   
//
//    public Funcionario getFuncionario() {
//        return funcionario;
//    }
//
//    public void setFuncionario(Funcionario funcionario) {
//        this.funcionario = funcionario;
//    }
//
//    public List<Funcionario> getLista() {
//        return this.dao.findAll();
//    }
//
//}
