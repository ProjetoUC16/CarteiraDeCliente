package br.com.rafael.rodrigo.carteiradecliente.dao;

import br.com.rafael.rodrigo.carteiradecliente.classe.Autor;



public class AutorDAO extends DAO<Autor> {

    public AutorDAO() {
        super(Autor.class);
    }

}
