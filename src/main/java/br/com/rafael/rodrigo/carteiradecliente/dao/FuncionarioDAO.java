package br.com.rafael.rodrigo.carteiradecliente.dao;

import br.com.rafael.rodrigo.carteiradecliente.classe.Funcionario;
import br.com.rafael.rodrigo.carteiradecliente.util.JPAUtil;
import javax.persistence.Query;

/**
 *
 * @author Diamond
 */
public class FuncionarioDAO extends DAO<Funcionario> {

    public FuncionarioDAO() {
        super(Funcionario.class);
    }

    public Funcionario findByLogin(Funcionario funcionario) {

        Funcionario f = null;
        try {
            this.em = JPAUtil.getEntityManager();
            this.em.getTransaction().begin();

            Query query = em.createQuery("from Funcionario f where f.cpf = " + funcionario.getCpf() + " and f.senha = " + funcionario.getSenha());
            f = (Funcionario) query.getSingleResult();

            this.em.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println("Erro na autenticacao");
        } finally {
            this.em.close();
        }

        return f;

    }

}
