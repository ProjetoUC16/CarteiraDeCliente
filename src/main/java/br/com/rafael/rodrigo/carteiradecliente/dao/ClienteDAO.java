
package br.com.rafael.rodrigo.carteiradecliente.dao;

import br.com.rafael.rodrigo.carteiradecliente.classe.Cliente;



public class ClienteDAO extends DAO<Cliente>{
    
    public ClienteDAO() {
        super(Cliente.class);
    }
    
}
