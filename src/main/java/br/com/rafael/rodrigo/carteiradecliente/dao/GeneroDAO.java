
package br.com.rafael.rodrigo.carteiradecliente.dao;

import br.com.rafael.rodrigo.carteiradecliente.classe.Genero;




public class GeneroDAO extends DAO<Genero>{
    
    public GeneroDAO() {
        super(Genero.class);
    }
    
}
