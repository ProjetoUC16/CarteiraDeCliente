package br.com.rafael.rodrigo.carteiradecliente.dao;

import br.com.rafael.rodrigo.carteiradecliente.classe.Vendas;



public class VendasDAO extends DAO<Vendas> {

    public VendasDAO() {
        super(Vendas.class);
    }

}
